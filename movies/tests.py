from config import base
import json
import requests
import unittest


class NewVisitorTest(unittest.TestCase):
    def test_can_add_a_film_and_retrieve_it_later_then_delete(self):
        # Frank has heard about a new cool RESTful Web Application on which you can
        # save your favourite Film and Tv series. He think for a while, then remember
        # the wonderful Tv series called "Lupin", he watched on Netflix.
        my_film = {
            "title": "Lupin",
            "director": "Louis Leterrier",
            "description": "Anni dopo una tragica ingiustizia, Assane vuole regolare i conti."
        }
        post_response = requests.post(f"{base.api_url}/movies/", json=my_film)

        pk = json.loads(post_response.content.decode("UTF-8"))['id']

        # When he hits Enter, the page updates and film is inserted.
        self.assertEqual(201, post_response.status_code)

        # After three months, Frank can't remember anymore the name of that beautiful Tv series he watched on Netflix.
        # But he remember he used to save his favourite ones on the Web App about movies. He goes to check out
        # the homepage. And here it is, Lupin!
        get_response = requests.get(f"{base.api_url}/movies/")
        self.assertEqual(200, get_response.status_code)

        film_list = get_response.json()
        self.assertIn(my_film['title'], [film['title'] for film in film_list])
        self.assertIn(my_film['director'], [film['director'] for film in film_list])
        self.assertIn(my_film['description'], [film['description'] for film in film_list])

        # Frank wants this film no longer be within his favourites list, so he  deletes it.
        delete_response = requests.delete(f"{base.api_url}/movies/{pk}/")
        self.assertEqual(204, delete_response.status_code)


if __name__ == '__main__':
    unittest.main()
